/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package Shapes;

public class Square extends shape{
    private double side;
      
    public Square (double side){
         this.side= side;
    }
    public double getSide() {
        return side;
    }
    
    @Override
    public double Area(){
        return Math.pow(side,2);
    }
   
    @Override
    public double Perimeter(){
    return 4*side;
    
}  
    @Override
    public String toString() {
        return " Squre Side=" + side + "\t " + "Area " + Area()+" \t "+ " Perimeter " + Perimeter();
    }
}
