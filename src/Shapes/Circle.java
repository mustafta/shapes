/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package Shapes;
public class Circle extends shape{
    
    public double radius;
     public Circle() {}
     
    public Circle(double radius) {
        this.radius = radius;
    }
    
    public double getRadius() {
        return radius;
    }
    public double Area(){
    return Math.PI * Math.pow(radius, 2);
    }
    
  public double Perimeter(){
  return 2* Math.PI *radius;
  }  

  @Override
    public String toString() {
        return " Circle radius " + radius + "\t"+ "Area " + Area()+ " \t "+ " Perimeter " + Perimeter();
    }
}